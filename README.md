# projectschool

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Info
Projeto desenvolvido atraves do curso ministrado pelo professor [Vinicius de andrade](https://www.youtube.com/user/ozirispc)
