import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueResoruce from 'vue-resource'

Vue.config.productionTip = false;
Vue.use(VueResoruce);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
