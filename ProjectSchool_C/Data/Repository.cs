using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectSchool_C.Models;

namespace ProjectSchool_C.Data
{
    public class Repository : IRepository
    {
        public DataContext _context { get; }

        public Repository(DataContext context)
        {
            _context = context;

        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public void Update<T>(T entity) where T : class
        {
            _context.Update(entity);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync() > 0);
        }

        //ALUNOS

        public async Task<AlunoModel[]> GetAllAlunosAsync(bool includeProfessor = false)
        {
            IQueryable<AlunoModel> query = _context.Alunos;

            if (includeProfessor)
            {
                query = query
                    .Include(pe => pe.Professor);
            }

            query = query.AsNoTracking()
                        .OrderBy(c => c.Id);

            return await query.ToArrayAsync();
        }


        public async Task<AlunoModel> GetAlunoAsyncById(int AlunoId, bool includeProfessor)
        {
            IQueryable<AlunoModel> query = _context.Alunos;

            if (includeProfessor)
            {
                query = query.Include(p => p.Professor);
            }

            query = query.AsNoTracking()
                        .OrderBy(aluno => aluno.Id)
                        .Where(aluno => aluno.Id == AlunoId);

            return await query.FirstOrDefaultAsync();
        }

        public async Task<AlunoModel[]> GetAlunosAsyncByProfessorId(int ProfessorId, bool includeProfessor)
        {
            IQueryable<AlunoModel> query = _context.Alunos;

            if (includeProfessor)
            {
                query = query
                    .Include(a => a.Professor);
            }

            query = query.AsNoTracking()
                        .OrderBy(a => a.Id)
                        .Where(al => al.ProfessorId == ProfessorId);

            return await query.ToArrayAsync();
        }




        //PROFESSORES
        public async Task<ProfessorModel[]> GetAllProfessoresAsync(bool includeAluno)
        {
            IQueryable<ProfessorModel> query = _context.Professores;

            if (includeAluno)
            {
                query = query
                    .Include(a => a.Alunos);
            }

            query = query.AsNoTracking()
                        .OrderBy(a => a.Id);
                        

            return await query.ToArrayAsync();
        }

        public async Task<ProfessorModel> GetProfessorAsyncById(int ProfessorId, bool includeAluno)
        {
            IQueryable<ProfessorModel> query = _context.Professores;

            if (includeAluno)
            {
                query = query.Include(p => p.Alunos);
            }

            query = query.AsNoTracking()
                        .OrderBy(p => p.Id)
                        .Where(prof => prof.Id == ProfessorId);

            return await query.FirstOrDefaultAsync();
        }
    }
}