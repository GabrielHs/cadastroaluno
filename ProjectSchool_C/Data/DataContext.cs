using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

using ProjectSchool_C.Models;



namespace ProjectSchool_C.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<AlunoModel> Alunos { get; set; }

        public DbSet<ProfessorModel> Professores { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //Para dar carga de valores no bd
            // builder.Entity<ProfessorModel>()
            // .HasData(
            //     new List<ProfessorModel>(){
            //         new ProfessorModel(){
            //             Id =1,
            //             Nome = "Gabriel"
            //         },
            //         new ProfessorModel(){
            //             Id =2,
            //             Nome = "Paula"
            //         },
            //         new ProfessorModel(){
            //             Id =3,
            //             Nome = "Luna"
            //         },
            //     }
            // );

            // builder.Entity<AlunoModel>()
            // .HasData(
            //     new List<AlunoModel>(){
            //         new AlunoModel(){
            //             Id =1,
            //             Nome = "Maria",
            //             Sobrenome = "José",
            //             DataNasc = "02/01/1981",
            //             ProfessorId = 1
            //         },
            //         new AlunoModel(){
            //             Id =2,
            //             Nome = "Ronaldinho",
            //             Sobrenome = "Gaucho",
            //             DataNasc = "10/12/1900",
            //             ProfessorId = 2
            //         },
            //         new AlunoModel(){
            //             Id =3,
            //             Nome = "Alex",
            //             Sobrenome = "Ferraz",
            //             DataNasc = "25/06/1997",
            //             ProfessorId = 3
            //         },
            //     }
            // );



            var listProfessores = new List<ProfessorModel>();
            var professor1 = new ProfessorModel() { Id = 1, Nome = "Gabriel" };
            var professor2 = new ProfessorModel() { Id = 2, Nome = "Paula" };
            var professor3 = new ProfessorModel() { Id = 3, Nome = "Luna" };
            listProfessores = new List<ProfessorModel>() { professor1, professor2, professor3 };

            var listAlunos = new List<AlunoModel>();

            var aluno1 = new AlunoModel() {
                        Id =1,
                        Nome = "Maria",
                        Sobrenome = "José",
                        DataNasc = "02/01/1981",
                        ProfessorId = 1
                    };
            var aluno2 = new AlunoModel() {
                        Id =2,
                        Nome = "Ronaldinho",
                        Sobrenome = "Gaucho",
                        DataNasc = "10/12/1900",
                        ProfessorId = 2
                    };
            var aluno3 = new AlunoModel() {
                        Id =3,
                        Nome = "Alex",
                        Sobrenome = "Ferraz",
                        DataNasc = "25/06/1997",
                        ProfessorId = 3
                    };
                    
            listAlunos = new List<AlunoModel>() { aluno1, aluno2, aluno3 };










            builder.Entity<ProfessorModel>().HasData(listProfessores.ToArray());
            builder.Entity<AlunoModel>().HasData(listAlunos.ToArray());

        }


    }
}