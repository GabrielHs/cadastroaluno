
using System.Threading.Tasks;
using ProjectSchool_C.Models;

namespace ProjectSchool_C.Data
{
    public interface IRepository
    {
        //GERAL
        void Add<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveChangesAsync();

        //ALUNO
        Task<AlunoModel[]> GetAllAlunosAsync(bool includeProfessor);
        Task<AlunoModel[]> GetAlunosAsyncByProfessorId(int ProfessorId, bool includeProfessor);
        Task<AlunoModel> GetAlunoAsyncById(int AlunoId, bool includeProfessor);

        //PROFESSOR
        Task<ProfessorModel[]> GetAllProfessoresAsync(bool includeAluno);
        Task<ProfessorModel> GetProfessorAsyncById(int ProfessorId, bool includeAluno);





    }
}