using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectSchool_C.Data;
using ProjectSchool_C.Models;
using ProjectSchool_C.Utils;

namespace ProjectSchool_C.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfessoresController : GabrielController
    {
        public IRepository _repo { get; }
        public ProfessoresController(IRepository repo)
        {
            _repo = repo;

        }
        [HttpGet]
        public async Task<IActionResult> Get()
        {


            try
            {
                var listaProfessores = await _repo.GetAllProfessoresAsync(true);

                return Ok(listaProfessores);
            }
            catch (System.Exception ex)
            {

                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro ao carrega lista professores -- " + ex.Message);
            }

        }



        [HttpGet("{professorId}")]
        public async Task<IActionResult> GetByProfessorId(int professorId)
        {
            try
            {
                var professor = await _repo.GetProfessorAsyncById(professorId, true);
                if (professor == null) return NotFound();

                return Ok(professor);
            }
            catch (System.Exception ex)
            {

                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro ao carrega professor -- " + ex.Message);
            }
        }




        [HttpPost]
        public async Task<IActionResult> Post(ProfessorModel model)
        {
            try
            {
                _repo.Add(model);
                if (await _repo.SaveChangesAsync())
                {
                    return Created($"/api/aluno/{model.Id}", model);
                }
            }
            catch (System.Exception ex)
            {

                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro ao criar professor" + ex.Message);
            }

            return BadRequest();
        }



        [HttpPut("{professorId}")]
        public async Task<IActionResult> Put(int professorId, ProfessorModel model)
        {
            try
            {
                var professor = await _repo.GetProfessorAsyncById(professorId, false);
                if (professor == null) return NotFound();

                _repo.Update(model);
                if (await _repo.SaveChangesAsync())
                {

                    return Created($"/api/aluno/{model.Id}", model);
                }
            }
            catch (System.Exception ex)
            {

                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro ao atualizar aluno" + ex.Message);
            }

            return BadRequest();
        }




        [HttpDelete("{professorId}")]
        public async Task<IActionResult> Delete(int professorId)
        {
            try
            {
                var professor = await _repo.GetProfessorAsyncById(professorId, false);
                if (professor == null) return NotFound();

                _repo.Delete(professor);

                if (await _repo.SaveChangesAsync())
                {
                    return Ok();
                }

            }
            catch (System.Exception ex)
            {

                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro ao excluir professor" + ex.Message);
            }

            return BadRequest();
        }















    }
}