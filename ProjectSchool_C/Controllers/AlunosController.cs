
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectSchool_C.Data;
using ProjectSchool_C.Models;
using ProjectSchool_C.Utils;

namespace ProjectSchool_C.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlunosController : GabrielController
    {
        public IRepository _repo { get; }
        public AlunosController(IRepository repo)
        {
            _repo = repo;

        }

        //Listagem do alunos
        [HttpGet]
        public async Task<IActionResult> Get()
        {


            try
            {
                var result = await _repo.GetAllAlunosAsync(true);
                return Ok(result);
            }
            catch (System.Exception ex)
            {

                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro ao carrega lista" + ex.Message);
            }


        }


        [HttpGet("ByProfessor/{ProfessorId}")]
        public async Task<IActionResult> GetProfessorId(int ProfessorId)
        {


            try
            {
                var result = await _repo.GetAlunosAsyncByProfessorId(ProfessorId, true);
                return Ok(result);
            }
            catch (System.Exception ex)
            {

                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro ao carrega professor" + ex.Message);
            }


        }


        [HttpGet("{alunoId}")]
        public async Task<IActionResult> GetByAlunoId(int alunoId)
        {


            try
            {
                var result = await _repo.GetAlunoAsyncById(alunoId, true);
                return Ok(result);
            }
            catch (System.Exception ex)
            {

                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro ao carrega aluno" + ex.Message);
            }


        }





        [HttpPost]
        public async Task<IActionResult> Post(AlunoModel model)
        {
            try
            {
                _repo.Add(model);
                if (await _repo.SaveChangesAsync())
                {
                    return Created($"/api/aluno/{model.Id}", model);
                }
            }
            catch (System.Exception ex)
            {

                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro ao criar aluno" + ex.Message);
            }

            return BadRequest();
        }



        [HttpPut("{alunoId}")]
        public async Task<IActionResult> Put(int alunoId, AlunoModel model)
        {
            try
            {
                var aluno = await _repo.GetAlunoAsyncById(alunoId, false);
                if (aluno == null) return NotFound();

                _repo.Update(model);
                if (await _repo.SaveChangesAsync())
                {
                    aluno = await _repo.GetAlunoAsyncById(alunoId, true);

                    return Created($"/api/aluno/{model.Id}", model);
                }
                return Ok();
            }
            catch (System.Exception ex)
            {

                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro ao atualizar aluno" + ex.Message);
            }
        }

        [HttpDelete("{alunoId}")]
        public async Task<IActionResult> Delete(int alunoId)
        {
            try
            {
                var aluno = await _repo.GetAlunoAsyncById(alunoId, false);
                if (aluno == null) return NotFound();

                _repo.Delete(aluno);

                return Ok();
            }
            catch (System.Exception ex)
            {

                return this.StatusCode(StatusCodes.Status500InternalServerError, "Erro ao excluir aluno" + ex.Message);
            }
        }







    }
}