using System.Collections.Generic;

namespace ProjectSchool_C.Models
{
    public class ProfessorModel
    {
       public int Id { get; set; }

        public string Nome {get;set;}

        public List<AlunoModel> Alunos { get; set; }



    }
}